import math as m

def binomial(n, k, p):
    return combinatorio(n,k) * p**k * (1-p)**(n-k)

def poisson(k, mu):
    return float(mu**k * m.exp(-mu)) / m.factorial(k)

def hipergeometrica(N,n,K,k):
    return combinatorio(N- K, n - k) * combinatorio (K, k)/ combinatorio(N,n)

def negBinomial(n, k, p):
    return combinatorio(n-1,k-1) * p**k * (1-p)**(n-k)

def combinatorio(n,k):
    return m.factorial(n) / (m.factorial(k) * m.factorial(n-k))
