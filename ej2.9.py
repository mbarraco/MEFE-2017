import math as m
import distributions as d
# import numpy as np

# 2.9.a
""" Distribucion Binomial
"""
resultado = 0
for k in range(0,3):
    resultado += d.binomial(10, k, .2)
print(1 - resultado)

# 2.9.b
resultado = d.binomial(6,1,.2)
print('2.9.a', resultado)

# 2.9.c
resultado = d.binomial(6,0,.2) * d.binomial(4,3,.2)
print('2.9.c', resultado)

