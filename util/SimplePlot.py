import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerLine2D
import sys
import os.path
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from util.SimpleGraph import SimpleGraph

class SimplePlot(SimpleGraph):

    DATA_Y = dict()
    DATA_X = []

    def setY(self, data, label):
        self.DATA_Y[label] = data
        return self

    def setX(self, data):
        self.DATA_X = data
        return self

    def _setDataToPlot(self):
        for key, value in self.DATA_Y.items():
            plt.plot(self.DATA_X, value, self.linestyle, label=key)


