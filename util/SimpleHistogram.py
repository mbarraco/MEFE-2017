import matplotlib.mlab as mlab
import sys
import os.path
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from util.SimpleGraph import SimpleGraph
import matplotlib.pyplot as plt
import numpy as np


class SimpleHistogram(SimpleGraph):

    def __init__(self, xlabel, ylabel):
        self.num_bins = 10
        super().__init__(xlabel, ylabel)

    def _setDataToPlot(self):
        weights = np.ones_like(self.data)/len(self.data)
        plt.hist(self.data, self.num_bins, weights=weights)

    def setBins(self, num_bins):
        self.num_bins = num_bins
        return self

    def setData(self, data):
        self.data = data
        return self

    def setCurve(self, xdata, ydata):
        plt.plot(xdata, ydata,'o-', lw=2)
        return self
