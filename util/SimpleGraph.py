import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerLine2D


class SimpleGraph:

    DATA_LABEL = []

    def __init__(self, xlabel, ylabel):
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.legLoc = 0
        self.linestyle = 'o-'
        self.title = ''


    def setLegendLocation(self, loc):
        self.legLoc = loc
        return self

    def setTitle(self, title):
        self.title = title
        return self

    def setLineStyle(self, lineStyle):
        self.linestyle = lineStyle
        return self

    def draw(self):
        self._setDataToPlot()
        plt.xlabel(self.xlabel)
        plt.ylabel(self.ylabel)
        plt.grid(True)
        if self.legLoc > 0:
            plt.legend(loc=self.legLoc, borderaxespad=0.)
        if len(self.title) > 0:
            plt.title(self.title)
        plt.show()

    def _setDataToPlot(self):
        pass
