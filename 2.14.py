import distributions as d

# 2.14.a
resultado = 0
for k in range(0,8):
    resultado += d.poisson(k, 2)
print('2.14.a  ', 1 - resultado)

# 2.14.b
print('2.14.a  ', 1 - d.binomial(365, 0, 0.0011))