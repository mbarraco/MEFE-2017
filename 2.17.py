import distributions as d
from util.SimplePlot import SimplePlot as sp


# 2.17.b
resultado = 0
for n in range(100,130):
    resultado += d.negBinomial(n, 100, .8)
print('2.17.b -->  ', 1 - resultado)

k = 100
p = .8

negBinomial = []
binomial = []
trials = range(100,150)
for n in trials:
    negBinomial.append(d.negBinomial(n,k,p))
    binomial.append(d.binomial(n,k,p))

# plot = sp('n', 'prob')
# plot.setX(trials).setY(negBinomial, 'NB').setY(binomial, 'B')
# plot.setTitle('2.17.b').draw()

# 2.17.c
suma = 0;
prob = []
trials =  range(100,150)

for n in trials:
    n_prob = 0
    for k in range(100,n):
        n_prob += d.binomial(n,k,p)
    prob.append(n_prob)
plot = sp('n', 'prob')
plot.setX(trials).setY(prob, 'p').setTitle('2.17.c').draw()