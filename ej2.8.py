import distributions as d

# 2.8.a
""" Distribucion de Poisson, lambda =  0.01 * 500
"""
resultado = 0
for k in range(0,10):
    resultado += d.poisson(k, 5)
print('2.8.a - poisson ', 1 - resultado)
resultado = 0
for k in range(0,10):
    resultado += d.binomial(500, k, .01)
print('2.8.a - binomial ', 1 - resultado)

# 2.8b

resultado = 0
for k in range(0,10):
    print(resultado)
    resultado += d.hipergeometrica(5000, 500, 50, k)
# print(resultado)
print('2.8.b ',1  - resultado)
