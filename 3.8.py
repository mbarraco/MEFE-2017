import distributions as d

# 3.8
resultado = 0
ns = range(1,20)
for n in ns:
    print(d.negBinomial(n, 1 , .05))
    resultado += d.negBinomial(n, 1 , .05)
print('3.8  ', resultado)