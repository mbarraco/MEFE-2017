import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import sys
import os.path
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from util.SimpleHistogram import SimpleHistogram as sh
import distributions as d



class ExperimentB:
    """Detector Bernoulli.
        n -> INCIDENT_PHOTONS
        p -> EFFICIENCY
    """

    INCIDENT_PHOTONS = 15
    EFFICIENCY = .75
    TRIAL_NUM = 1000;

    def runExperiment(self):
        self.resultados = []
        for i in range(0, self.TRIAL_NUM):
            self.resultados.append(len([x for x in np.random.uniform(0, 1, self.INCIDENT_PHOTONS) if x < self.EFFICIENCY]))

    def showResult(self):
        num_bins = 20
        distro = []
        xspan = range(0,  self.INCIDENT_PHOTONS + 1)
        for k in xspan:
            distro.append(d.binomial(self.INCIDENT_PHOTONS, k, self.EFFICIENCY))
        plot = sh('ocurrencia', 'cantidad')
        plot.setData(self.resultados).setBins(num_bins).setCurve(xspan, distro).setTitle('2.20 "B"').draw()


class ExperimentC:
    """Poisson de INTENSIDAD eventos por segundo.
        mu -> INTENSIDAD
    """

    INTENSIDAD = 15 # [intensidad] = 1/s
    TIME_SPAN = 1;
    TIME_SLOTS = 1000

    def runExperiment(self):
        self.resultados = []
        for i in range (0, self.TIME_SLOTS + 1):
            self.resultados.append(len([x for x in np.random.uniform(0, 1, self.TIME_SLOTS) if x < self.INTENSIDAD / self.TIME_SLOTS]))
        return self.resultados

    def showResult(self):
        num_bins = 100
        distro = []
        xspan = range(0, 4 * self.INTENSIDAD) # Grafico de 0 a 4 lambdas
        for k in xspan:
            distro.append(d.poisson(k, self.INTENSIDAD))
        plot = sh('ocurrencia', 'cantidad')
        plot.setData(self.resultados).setBins(num_bins).setCurve(xspan, distro).setTitle('2.20 "C"').draw()


class ExperimentD:
    """ Detector de A con fuente de B"""

    def __init__(self):
        self.emmitted_photons = ExperimentC().runExperiment() # ExperimentB.TRIAL_NUM
        self.detector_eff = ExperimentB.EFFICIENCY

    def runExperiment(self):
        self.resultados = []
        for photons in self.emmitted_photons:
            self.resultados.append(len([x for x in np.random.uniform(0, 1, photons) if x < self.detector_eff]))
        return self.resultados

    def showResult(self):
        num_bins = 100
        distro = []
        xspan = range(0, round(4 * self.detector_eff * ExperimentC.INTENSIDAD)) # Grafico de 0 a 4 lambdas de la distro efectiva
        for k in xspan:
            distro.append(d.poisson(k, self.detector_eff * ExperimentC.INTENSIDAD ))
        plot = sh('ocurrencia', 'cantidad')
        plot.setData(self.resultados).setBins(num_bins).setCurve(xspan, distro).setTitle('2.20 "D"').draw()

class ExperimentE:
    """ Composición B y C"""

    INTENSIDAD = ExperimentC.INTENSIDAD * ExperimentB.EFFICIENCY
    TIME_SPAN = 1;
    TIME_SLOTS = 1000

    def runExperiment(self):
        self.resultados = []
        for i in range (0, self.TIME_SLOTS + 1):
            self.resultados.append(len([x for x in np.random.uniform(0, 1, self.TIME_SLOTS) if x < self.INTENSIDAD / self.TIME_SLOTS]))
        return self.resultados

    def showResult(self):
        num_bins = 100
        distro = []
        xspan = range(0, 4 * ExperimentC.INTENSIDAD) # Grafico de 0 a 4 lambdas
        for k in xspan:
            distro.append(d.poisson(k, self.INTENSIDAD))
        plot = sh('ocurrencia', 'cantidad')
        plot.setData(self.resultados).setBins(num_bins)
        plot.setCurve(xspan, distro).setTitle('2.20 "C"')
        plot.draw()

def evalExperiment(experiment):
    experiment.runExperiment()
    experiment.showResult()

evalExperiment(ExperimentE())




