import numpy as np
import matplotlib.mlab as mlab
import sys
import os.path
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from util.SimpleHistogram import SimpleHistogram as sh



class Experiment():

    TRIALS = 500000
    XMIN = 0
    XMAX = 1.5

    def runExperiment(self):
        a = 1
        b = 0
        transform = lambda x: a * np.tan(x) + b
        self.transformed = list(map(transform, np.random.uniform(self.XMIN, self.XMAX, self.TRIALS)))
        cauchy = lambda x: a / (np.pi**2 * (a + (x-b)**2))
        self.xspan = np.linspace(self.XMIN, self.XMAX, 100)
        self.distro = list(map(cauchy, self.xspan));


    def showResult(self):
        num_bins = 100
        distro = []
        xspan = range(0, ) # Grafico de 0 a 4 lambdas de la distro efectiva
        plot = sh('ocurrencia', 'cantidad')
        plot.setData(self.transformed).setBins(num_bins)
        cauchy = lambda x: 1 / (np.pi * (1 + x**2))
        plot.setCurve(self.xspan, self.distro)
        plot.setTitle('3.10').draw()


def evalExperiment(experiment):
    experiment.runExperiment()
    experiment.showResult()

evalExperiment(Experiment())