import numpy as np
import matplotlib.mlab as mlab
import sys
import os.path
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from util.SimplePlot import SimplePlot as sp



class ExperimentA():

    def __init__(self):
        self.range = 15

    def runExperiment(self):
        self.xs = np.linspace(-self.range, self.range, 300)
        sigma = .75
        mu = 0
        cauchy = lambda x: 1 / (np.pi * (1 + x**2))
        gauss = lambda x: np.exp(-.5 * ((x - mu) / sigma)**2) / (np.sqrt(2*np.pi) * sigma)
        normalization = cauchy(0) / gauss(0)
        self.gaussian = list(map(getGaussianHandle(mu, sigma, normalization), self.xs))
        self.cauchy = list(map(cauchy, self.xs))


    def showResult(self):
        plot = sp('', 'x')
        plot.setX(self.xs).setY(self.gaussian, 'Gaussian').setY(self.cauchy, 'Cauchy')
        plot.setTitle('3.9.a').setLineStyle('-').setLegendLocation(2).draw()


class ExperimentB:

    def __init__(self):
        expA = ExperimentA()
        expA.runExperiment()
        self.cauchy = expA.cauchy
        self.range = expA.range

    def runExperiment(self):
        self.xs = np.linspace(-self.range, self.range, 300)
        a = .5
        sigma1 = .75
        sigma2 = 3
        gaussian1 = list(map(getGaussianHandle(0, sigma1, 1), self.xs))
        gaussian2 = list(map(getGaussianHandle(0, sigma2, 1), self.xs))
        self.gaussian = [sum(x) for x in zip(gaussian1, gaussian2)]
        self.gaussian = list(map(lambda x: x * np.amax(self.cauchy) / np.amax(self.gaussian), self.gaussian))

    def showResult(self):
        plot = sp('', 'x')
        plot.setX(self.xs).setY(self.gaussian, 'Gaussian').setY(self.cauchy, 'Cauchy')
        plot.setTitle('3.9.b').setLineStyle('-').setLegendLocation(2).draw()


def getGaussianHandle(mu, sigma, normalization):
    handle = lambda x: normalization * np.exp(-.5 * ((x - mu) / sigma)**2) / (np.sqrt(2*np.pi) * sigma)
    return handle

def evalExperiment(experiment):
    experiment.runExperiment()
    experiment.showResult()

evalExperiment(ExperimentB())